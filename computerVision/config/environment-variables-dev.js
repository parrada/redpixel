'use strict'
module.exports = {
    port : '8082',
    google_application_credentials : './security/certificates-and-keys/redpixel-computer-vision.json'
}