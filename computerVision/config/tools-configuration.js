'use strict'
/** import libraries an dependencies section */
const visionLibrary = require('@google-cloud/vision');
const express = require('express');

module.exports = {
    vision : {
        config: new visionLibrary.ImageAnnotatorClient()
    },
    express:{
        config: express()
    }
}