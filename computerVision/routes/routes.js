const globalResources = require('../config/tools-configuration');
const services = require('../services/service');

//routes
globalResources.express.config.get('/objectDetection', services.objectDetection);
globalResources.express.config.get('/logoDetection', services.logoDetection);
globalResources.express.config.post('/labelsValidation', services.labelsValidation);
globalResources.express.config.post('/textDetection', services.textDetection);
module.exports = globalResources;