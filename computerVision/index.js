'use strict'

/** import libraries an dependencies section */
const globalResources = require('./boot/app.js');
const environment = require('./config/environment-variables-dev');

/** environment variables manual setting */
//process.env.PORT = environment.port;
process.env.GOOGLE_APPLICATION_CREDENTIALS = environment.google_application_credentials;
//process.env.STORAGE_SERVICE = ''
//process.env.IMAGE_FILTERS = ['Drink', 'Bottle', 'Cola', 'Soft drink', 'Carbonated soft drinks', 'Coca-cola'];

/** RUN SERVER */
globalResources.express.config.listen(process.env.PORT, function(){
    console.log('Example app listening on port  ' + process.env.PORT);
});