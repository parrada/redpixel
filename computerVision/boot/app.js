/** import libraries an dependencies section */
const bodyParser = require('body-parser');
const globalResources = require('../config/tools-configuration');
var express = require('express');
var path = require('path');

//middlewares
globalResources.express.config.use(bodyParser.json({
    limit: '150mb',
    extended: true,
    type: 'application/json',
    parameterLimit: 9000000
}));

globalResources.express.config.use(bodyParser.urlencoded({
    limit: 100000,
    extended: true
}));

globalResources.express.config.use(express.json());
globalResources.express.config.use(express.urlencoded({
    extended: true
}));
globalResources.express.config.use(express.static(path.join(__dirname, 'public')));

/** routes */
require('../routes/routes');

module.exports = globalResources;