const repository = require('../repository/repository');
const storage = require('../models/storage/interface');
let services = {};

/**************************** UTILITARY METHODS **************************************** */

const buildLambdaRequest = (payload, operation, destination) => {
    return {
        destination,
        payload: {
            operation,
            data: payload
        }
    };
};

const generateRandom = () => {
    let number = Math.floor(Math.random() * (50000 - 1 + 1) + 1);
    console.log('[NUMBER]', number)
    return number.toString()
};

const serviceResponse = (res, validationForm) => {
    let ok = false;
    Object.keys(validationForm).forEach(prop => {
        if (validationForm[prop]['checked'] == false) {
            ok = true;
        }
    });
    if (ok == false) {
        res.status(200).send({
            status: 'the image is valid'
        });
    } else {
        res.status(400).send({ status: 'the image isn\'t valid' });
    }
}

const uploadStorageFiles = data => {
    return new Promise((resolve, reject) => {
        let promisesList = [];
        /** get the images stored in s3 */
        data.forEach(experience => {
            /** get the image reference*/
            let lambdaPayload = {
                bucketName: process.env.BUCKET_NAME,
                fileName: experience.label,
                type: 'user/computer-vision',
                payload: experience.payload
            }
            let dataRequest = buildLambdaRequest(lambdaPayload, 'upload', process.env.STORAGE_SERVICE);
            /** get the files located in the storage service */
            promisesList.push(storage.uploadFiles(dataRequest));
        });
        Promise.all(promisesList).then(resMedia => {
            console.log('[MEDIA COMPLETED]')
            resolve(resMedia);
        }).catch(err => reject(err));
    });
};

/**************************** END UTILITARY METHODS ************************************ */


services.logoDetection = (req, res) => {
    repository.method(req.body).then(result => {
        res.status(200).send(result);
    }).catch(err => res.status(400).send(err));
};

services.objectDetection = (req, res) => {
    repository.method(dreq.body).then(res => {
        res.status(200).send(result);
    }).catch(err => res.status(400).send(err));
};
services.textDetection = (req, res) => {
    repository.textDetection(req.body)
        .then(resTextDetection => {
            return res.status(200).send({ description: resTextDetection[0].description });
        })
        .catch(errTextDetection => {
            return res.status(400).send({ error: errTextDetection });
        });
}
services.labelsValidation = async (req, res) => {
    repository.labelsValidation(req.body).then(labels => {

        let filters = JSON.parse(process.env.IMAGE_FILTERS);

        let validationForm = {};
        /** convert the initial filters to a validation form */
        filters.forEach(filter => {
            validationForm[filter] = { checked: false };
        });

        let validationResult = labels.filter(l => filters.includes(l.description));

        validationResult.forEach(item => {

            let labelId = item['description'];
            validationForm[labelId]['checked'] = true;
        })

        /** save the image in the storage service */
        let storageFile = [];
        /** set the image name */
        let fileName = `usr_${req.body.userId}_img_${generateRandom()}`

        let fileDataToUpload = {
            label: fileName,
            payload: req.body.payload
        };
        storageFile.push(fileDataToUpload);

        /** make the request to upload files */
        try {
            await uploadStorageFiles(storageFile);
            serviceResponse(res, validationForm);
        } catch (error) {
            console.log('[ERROR SAVING THE IMAGE]', error);
            serviceResponse(res, validationForm);
        }
    }).catch(err => res.status(400).send(err));
};

module.exports = services;