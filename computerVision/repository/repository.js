'use strict'
const client = require('../config/tools-configuration').vision.config;
const repository = {};

repository.logoDetection = data => {
    return new Promise(async (resolve, reject) => {
        try {
            // Performs label detection on the image file
            const request = {
                image: {
                    "content": data.payload
                }
            };
            const [result] = await client.logoDetection(request);
            const logos = result.logoAnnotations;
            console.log('Logos:');
            logos.forEach(logo => console.log(logo));

            resolve(logos);
        } catch (error) {
            reject(error);
        }
    });
};

repository.objectDetection = data => {
    return new Promise(async (resolve, reject) => {
        try {
            //const fileName = `./resources/repisa.jpg`;
            const request = {
                image: {
                    "content": data.payload
                }
            };

            const [result] = await client.objectLocalization(request);
            const objects = result.localizedObjectAnnotations;
            objects.forEach(object => {
                for (let key in Object.getOwnPropertyNames) { console.log(key) }
                //console.log(`object: ${object.toString()}`);
                //console.log(`Confidence: ${object.score}`);
                const vertices = object.boundingPoly.normalizedVertices;
                //vertices.forEach(v => console.log(`x: ${v.x}, y:${v.y}`));
            });

            resolve(objects);
        } catch (error) {
            reject(error);
        }

    });
};

repository.labelsValidation = data => {
    return new Promise(async (resolve, reject) => {
        try {
            console.log('[DATA]', data.payload.length)
            const request = {
                image: {
                    "content": data.payload
                }
            };
            // Performs label detection on the local file
            const [result] = await client.labelDetection(request);
            const labels = result.labelAnnotations;
            /*          console.log('Labels:');
                        labels.forEach(label => console.log(label.description)); */

            resolve(labels);
        } catch (error) {
            reject(error);
        }
    });
};
repository.textDetection = data => {
    return new Promise(async (resolve, reject) => {
        try {
            const request = {
                image: {
                    "content": data.payload
                }
            };
            const [result] = await client.textDetection(request);
            const detections = result.textAnnotations;
            detections.forEach(text => console.log(text));

            resolve(detections);
        } catch (error) {
            reject(error);
        }
    });
};

module.exports = repository;