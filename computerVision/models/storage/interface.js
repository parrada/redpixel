let interfaceMethods = {};
const implementations = {
    s3: require('./model')
};

interfaceMethods.getFiles = (data, fileName) => {
    return new Promise((resolve, reject) => {
        implementations.s3.getFiles(data,fileName)
            .then(res => resolve(res))
            .catch(err => reject(err));
    });
};

interfaceMethods.uploadFiles = data => {
    return new Promise((resolve, reject) => {
        implementations.s3.uploadFiles(data)
            .then(res => resolve(res))
            .catch(err => reject(err));
    });
};

interfaceMethods.removeFiles = data => {
    return new Promise((resolve, reject) => {
        implementations.s3.removeFiles(data)
            .then(res => resolve(res))
            .catch(err => reject(err));
    });
};

module.exports = interfaceMethods;