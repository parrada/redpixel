let storageController = {};
var AWS = require('aws-sdk');
AWS.config.region = 'us-east-1';
var lambda = new AWS.Lambda();

storageController.getFiles = (data, fileName) => {
    return new Promise((resolve, reject) => {
        const params = {
            FunctionName: data.destination, // the lambda function we are going to invoke
            InvocationType: 'RequestResponse',
            LogType: 'Tail',
            Payload: JSON.stringify(data.payload) //the list of files that we want to get
        };
        lambda.invoke(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                if(data['StatusCode'] !== 200){
                    reject(data['stack']);
                }
                let body = {
                    label : fileName,
                    payload : JSON.parse(data['Payload'])
                };
                
                resolve(body);
            }
        });

    });
};

storageController.uploadFiles = data => {
    return new Promise((resolve, reject) => {
        const params = {
            FunctionName: data.destination, // the lambda function we are going to invoke
            InvocationType: 'RequestResponse',
            LogType: 'Tail',
            Payload: JSON.stringify(data.payload) //the list of files that we want to upload
        };

        lambda.invoke(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                if(data['StatusCode'] !== 200){
                    reject(data['stack']);
                }
                let body = JSON.parse(data['Payload']);                
                resolve(body);
            }
        });

    });
};

storageController.removeFiles = data => {
    return new Promise((resolve, reject) => {
        const params = {
            FunctionName: data.destination, // the lambda function we are going to invoke
            InvocationType: 'RequestResponse',
            LogType: 'Tail',
            Payload: JSON.stringify(data.payload) //the list of files that we want to upload
        };

        lambda.invoke(params, (err, data) => {
            if (err) {
                reject(err);
            } else {
                if(data['StatusCode'] !== 200){
                    reject(data['stack']);
                }
                let body = JSON.parse(data['Payload']);                
                resolve(body);
            }
        });

    });
};

module.exports = storageController;